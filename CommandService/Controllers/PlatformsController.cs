﻿using AutoMapper;
using CommandService.Data;
using CommandService.Dtos;
using CommandService.SyncDataServices.Grpc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CommandService.Controllers
{
    [Route("api/c/[controller]/[action]")]
    [ApiController]
    public class PlatformsController : ControllerBase
    {
        private readonly ICommandRepo _commandRepo;
        private readonly IMapper _mapper;
        private readonly IPlatformDataClient _grpcClient;
        public PlatformsController(ICommandRepo commandRepo, IMapper mapper, IPlatformDataClient grpcClient)
        {
            _commandRepo = commandRepo;
            _mapper = mapper;
            _grpcClient = grpcClient;
        }

        [HttpGet]
        public ActionResult<IEnumerable<PlatformreadDto>> GetPlatforms()
        {
            Console.WriteLine("--> Getting Platforms from CommandService");
            var platformLists = _commandRepo.GetAllPlatforms();
            return Ok(_mapper.Map<IEnumerable<PlatformreadDto>>(platformLists));
        }

        [HttpPost]
        public ActionResult SyncPlatforms()
        {
            try
            {
                var platforms = _grpcClient.ReturnAllPlatforms();
                if (platforms == null)
                    return Ok("Grpc not connected");
                PrepDb.SeedData(_commandRepo, platforms);
                return Ok("Platforms Sync");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Exception: {ex.Message}");
                return BadRequest(ex.Message);
            }
        }
    }
}
